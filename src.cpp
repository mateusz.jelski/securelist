#define _CRT_SECURE_NO_WARNINGS
#include <cstdio>
#include <cstdlib>
#include <string>

struct SC
{
	SC* prev;
	SC* next;

	const char* szId;

	int index;
	void* data;
};

struct SC_LIST_HEAD
{
	SC* next;
};

struct SC_DATA
{
	const char* szId;

	int index;
	void* data;
};

struct SC_ITERATOR
{
	SC_ITERATOR* prev;
	SC_ITERATOR* next;

	const char* szId;
};

struct SC_STATIC_HEAD
{
	SC_ITERATOR next;
};

SC_ITERATOR ScNext(SC_ITERATOR i)
{
	return *i.next;
}

void ScAddNewNode(SC_LIST_HEAD* head, int index, void* data, const char* szIdent)
{
	if (head->next == nullptr)
	{
		head->next = new SC;
		head->next->prev = nullptr;
		head->next->next = nullptr;

		head->next->index = index;
		head->next->data = data;
		head->next->szId = szIdent;
		return;
	}

	SC* cSC = head->next;

	while (true)
	{
		if (cSC->next == nullptr)
		{
			cSC->next = new SC;
			cSC->next->prev = cSC;
			cSC->next->next = nullptr;
			cSC->next->index = index;
			cSC->next->data = data;
			cSC->next->szId = szIdent;
			return;
		}
		cSC = cSC->next;

	}
}

#define SC_ITER(P)           (SC_ITERATOR*) (P.next)
#define SC_GET_DATA(P)       (SC_DATA*)     ((char*)P + (sizeof(SC*) * 2) )
#define SC_GET_FULL_DATA(P)  (SC*)          (P)

const char* DynamicString(size_t size, const char* str)
{
	const char* mm = new char[size];
	strcpy((char*)mm, str);
	return mm;
}

int main()
{
	SC_LIST_HEAD head;
	head.next = nullptr;


	ScAddNewNode(&head, 0, nullptr, DynamicString(32, "DATA0"));
	ScAddNewNode(&head, 1, nullptr, DynamicString(32, "DATA1"));
	ScAddNewNode(&head, 2, nullptr, DynamicString(32, "DATA2"));
	ScAddNewNode(&head, 3, nullptr, DynamicString(32, "DATA3"));


	SC_ITERATOR LE{};
	memcpy(&LE, head.next, sizeof(void*) * 3);


	SC_ITERATOR i = LE;
	while (true)
	{
		if (i.next == nullptr)
			break;
		
		if (strcmp(i.szId, "DATA2") == 0)
		{
			printf("Found.\n");
			break;
		}
		
		i = ScNext(i);
	}
}